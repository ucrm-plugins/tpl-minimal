<?php
/**
 * @noinspection DuplicatedCode
 * @noinspection PhpIncludeInspection
 * @noinspection PhpMultipleClassDeclarationsInspection
 * @noinspection PhpUnhandledExceptionInspection
 * @noinspection PhpUnusedParameterInspection
 */
declare(strict_types=1);

require_once __DIR__."/vendor/autoload.php";

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use SpaethTech\UCRM\SDK\Kernels\InternalKernel;
use SpaethTech\UCRM\SDK\Kernels\Kernel;

return [
    Kernel::class => DI\create(InternalKernel::class)
        ->constructor(DI\get(ContainerInterface::class)),

    LoggerInterface::class => DI\create(Logger::class)
        ->constructor("plugin")
        ->method("pushHandler",
            new StreamHandler(__DIR__."/data/plugin.log")),

    // ...
];
