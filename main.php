<?php
/**
 * @noinspection DuplicatedCode
 * @noinspection PhpIncludeInspection
 * @noinspection PhpMultipleClassDeclarationsInspection
 * @noinspection PhpUnhandledExceptionInspection
 */
declare(strict_types=1);

require_once __DIR__."/vendor/autoload.php";

use DI\ContainerBuilder;
use SpaethTech\UCRM\SDK\Kernels\Kernel;
use SpaethTech\UCRM\SDK\Plugin;
use SpaethTech\UCRM\SDK\Server;

$builder = new ContainerBuilder();
$builder->addDefinitions("config.php");
$container = $builder->build();

$server = new Server($container->get(Kernel::class));
$plugin = new Plugin($server, __DIR__);
$logger = $plugin->getLogger();

$logger->info("This plugin has no manual or scheduled functionality!");
